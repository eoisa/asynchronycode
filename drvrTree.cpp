//=======================================================
//	Eyad isa
//	Data Structures II
//	Assignment 06
//	21 December 2014
//=======================================================
#include <iostream>
#include <iomanip>
#include "Tree.h"
using namespace std;

void deleteVal(Tree<char>&), addVal(Tree<char>&),
	searchVal(Tree<char>&), nodeNumb(Tree<char>&),
	smallVal(Tree<char>&), largeVal(Tree<char>&),
	treeHeight(Tree<char>&), ascendTree(Tree<char>&),
	preTree(Tree<char>&), postTree(Tree<char>&),
	graphTree(Tree<char>&);
bool checkChar();
int displayMainMenu();

int main()
{
	Tree<char> bTree;
	bool terminator = false;
	int intVal = ' ';
	while(!terminator)
	{
		intVal = displayMainMenu();
		switch (intVal)
		{
		case 1:
			addVal(bTree);
			break;
		case 2:
			deleteVal(bTree);
			break;
		case 3:
			searchVal(bTree);
			break;
		case 4:
			nodeNumb(bTree);
			break;
		case 5:
			smallVal(bTree);
			break;
		case 6:
			largeVal(bTree);
			break;
		case 7:
			treeHeight(bTree);
			break;
		case 8:
			ascendTree(bTree);
			break;
		case 9:
			preTree(bTree);
			break;
		case 10:
			postTree(bTree);
			break;
		case 11:
			graphTree(bTree);
			break;
		default:
			terminator = true;
		}
	}
	return 0;
}

int displayMainMenu()
{
	int selection;
	cout << "Choose your operation:" << endl;
	cout << "Operation		Code\n" << endl;
	cout << "Add			1\nDelete			2\nSearch			3\nNode Count		4\nSmallest Value		5\nLargest Value		6\nTree Height		7\nAscending Display	8\n"
		"Preorder Display	9\nPostorder Display	10\nGraphic Tree		11\nQuit			12" << endl;
	cin >> selection;

	while(selection < 1 || selection > 12)
	{
		cout << "The operation code you entered was invalid." << endl;
		cout << "Please choose a valid operation code:" << endl;
		cout << "Operation		Code\n" << endl;
		cout << "Add			1\nDelete			2\nSearch			3\nNode Count		4\nSmallest Value		5\nLargest Value		6\nTree Height		7\nAscending Display	8\n"
			"Preorder Display	9\nPostorder Display	10\nGraphic Tree		11\nQuit			12" << endl;
		cin >> selection;
	}
	
	return selection;
}

void addVal(Tree<char>& bTree)
{
	bool found = false;
	char inChar = 'a';
	while(isalnum(inChar))
	{
		cout << "Input a character to add, press '+' after final entry." << endl;
		cin >>  inChar;
		if(inChar == '+')
		{
			break;
		}
		found = bTree.tSearch(inChar, found);
		if(found)
		{
			cout << "\nError: Character already in tree." << endl;
		}
		else
		{
			
			bTree.NewNode(inChar);
		}
	}
}

void deleteVal(Tree<char>& bTree)
{
	bool found = true;
	char inChar = 'a';
	while(isalnum(inChar))
	{
		cout << "Input a character to delete, press '+'  after final entry." << endl;
		cin >>  inChar;
		if(inChar == '+')
		{
			break;
		}
		found = bTree.tSearch(inChar, found);
		if(!found)
		{
			cout << "\nError: Character is not in tree." << endl;
		}
		else
		{
			bTree.tDelete(inChar);
		}
	}
}

void searchVal(Tree<char>& bTree)
{
	char inChar = 'a';
	bool found;
	while(isalnum(inChar))
	{
		found = false;
		cout << "Input a character to search for, press '+' after final entry." << endl;
		cin >>  inChar;
		if(inChar == '+')
		{
			break;
		}
		found = bTree.tSearch(inChar,found);
		if(found)
		{
			cout << inChar << " was found." << endl;
		}
		else
		{
			cout << "Error: Character does not exist." << endl;
		}
	}
}

void nodeNumb(Tree<char>& bTree)
{
	cout << "There are " << bTree.tCount() << " nodes in the tree." << endl;
}

void smallVal(Tree<char>& bTree)
{
	treeNode* smallest = bTree.tSmall();	//	gets smallest node
	cout<<"Smallest is "<<smallest->key<<endl;
}

void largeVal(Tree<char>& bTree)
{
	treeNode* largest= bTree.tLarge();	//	gets largest node
	cout<<"Largest is "<<largest->key<<endl;
}

void treeHeight(Tree<char>& bTree)
{
	cout << "The tree is " << bTree.tCount() << " nodes high." << endl;
}

void ascendTree(Tree<char>& bTree)
{
	bTree.tInOrder();
	cout << endl;
}

void preTree(Tree<char>& bTree)
{
	bTree.tPreOrder();
	cout << endl;
}

void postTree(Tree<char>& bTree)
{
	bTree.tPostOrder();
	cout << endl;
}

void graphTree(Tree<char>& bTree)
{
	int indent;
	cout << "Please input you deired indent:" << endl;
	cin >> indent;
	bTree.tGraph(indent);
}