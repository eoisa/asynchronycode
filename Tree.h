//=======================================================
//	Eyad isa
//	Data Structures II
//	Assignment 06
//	21 December 2014
//=======================================================
using namespace std;

struct treeNode
{
    treeNode* left;
    char key;
    treeNode* right;
};

template <class DT>
class Tree
{
public:  
	Tree();
	void NewNode(char);
	treeNode* tLarge ();
	treeNode* tSmall ();
	bool tSearch(char&, bool&);
	void tDelete(char);
	int tHeight(int, int);
	void tGraph(int);
	int tCount();
	void tPreOrder();
	void tPostOrder();
	void tInOrder();
private:
	treeNode* largestNode (treeNode*);
	treeNode* smallestNode (treeNode*);
	int countNodes(treeNode*);
	void preOrder(treeNode*);
	void postOrder(treeNode*);
	void inOrder(treeNode*);
	void graphAux(int, treeNode*);
	void addBST(treeNode*&, treeNode*);
	bool searchTree(treeNode*, char&, bool&);
	void deleteBST(treeNode*&, char);
	int height(treeNode*);
	int max(int, int);
	treeNode* tree;
	treeNode* pNew;
};

//==================== Public Functions ====================// 

template <class DT>
Tree<DT>::Tree()
{
	tree = NULL;
}

template <class DT>
void Tree<DT>::NewNode(char inChar)
{
	pNew = new treeNode;
	pNew->key = inChar;
	pNew->right = NULL;
	pNew->left = NULL;

	addBST(tree, pNew);
}

template <class DT>
void Tree<DT>::tPreOrder()
{
	preOrder(tree);
}

template <class DT>
void Tree<DT>::tPostOrder()
{
	postOrder(tree);
}

template <class DT>
void Tree<DT>::tInOrder()
{
	inOrder(tree);
}

template <class DT>
treeNode* Tree<DT>::tSmall()
{
	return smallestNode (tree);
}

template <class DT>
treeNode* Tree<DT>::tLarge()
{
	return largestNode (tree);
}

template <class DT>
bool Tree<DT>::tSearch(char& item, bool& found)
{
	return searchTree(tree, item, found);
}

template <class DT>
void Tree<DT>::tDelete(char item)
{
	deleteBST(tree, item);
}

template <class DT>
int Tree<DT>::tCount()
{
	return countNodes(tree);
}

template <class DT>
void Tree<DT>::tGraph(int indent)
{
	graphAux(indent, tree);
}

template <class DT>
int Tree<DT>::tHeight(int first, int second)
{
	max(first, second);
	height(tree);
}

//==================== Private Functions ====================// 

template <class DT>
void Tree<DT>::addBST(treeNode*& root, treeNode* newNode)
{
	if (root == NULL)
	   root = newNode;
	else
	   if (newNode->key < root->key)
		   addBST(root->left, newNode);
	   else
		   addBST(root->right, newNode);
}

//***  preOrder Traversal
template <class DT>
void Tree<DT>::preOrder(treeNode* root)
{
	if (root!=NULL)
	{
		cout<<root->key;
		preOrder(root->left);
		preOrder(root->right);
	}
}

//***  postOrder Traversal
template <class DT>
void Tree<DT>::postOrder(treeNode* root)
{
	if (root!=NULL)
	{
		postOrder(root->left);
		postOrder(root->right);
		cout<<root->key;
		
	}
}

//***  inOrder Traversal
template <class DT>
void Tree<DT>::inOrder(treeNode* root)
{
	if (root!=NULL)
	{
		inOrder(root->left);
		cout<<root->key;
		inOrder(root->right);
	}
}

//***  Find the smallest value
template <class DT>
treeNode* Tree<DT>::smallestNode (treeNode* root)
{
	if (root->left == NULL)
	{
		return root;
	}
	else
	{
		return smallestNode(root->left);
	}
}

//***  Find the largest value
template <class DT>
treeNode* Tree<DT>::largestNode (treeNode* root)
{
	if (root->right == NULL)
	{
		return root;
	}
	else
	{
		return largestNode(root->right);
	}
}

//***   Search for a value in the tree
template <class DT>
bool Tree<DT>::searchTree(treeNode* root, char& item, bool& found)
{
    if (root == NULL)
		found = false;		// item is not found.
    else if (item < root ->key)	// Search left subtree.
	  	searchTree (root ->left, item, found);  
	else if (item > root ->key)	// Search right subtree
		searchTree (root ->right, item, found); 
    else
    {
	item = root ->key;			// item is found.
	found = true;
    }
	return found;
}

// Deletes item from tree.
template <class DT>
void Tree<DT>::deleteBST(treeNode*& root, char item)
{
	treeNode* tempPtr;
	if (item < root->key)
	{
		deleteBST(root->left, item);
	}
	else if (item > root->key)
	{
		deleteBST(root->right, item);
	}
	else   //****   item==root->key  *******
	{
		if (root->left == NULL)
		{
			tempPtr=root;
			root = root->right;
			delete tempPtr;
		}
		else if (root->right == NULL)
		{
			tempPtr=root;
			root = root->left;
			delete tempPtr;
		}
		else
		{
			tempPtr=root->left;
			while(tempPtr->right != NULL)
			{
				tempPtr=tempPtr->right;
			}
			root->key = tempPtr->key;
			deleteBST(root->left,tempPtr->key);
		}
	}
}

//***  Count Nodes
template <class DT>
int Tree<DT>::countNodes(treeNode* root)
{
    if (root == NULL)
	{
		return 0;
	}
	else
	{
		//  returns the number of nodes in the tree.
		return  countNodes(root->left) + countNodes(root->right) + 1;
	}	
}

//***   Display the tree graphically
template <class DT>
void Tree<DT>::graphAux(int indent, treeNode* root)
{
  if (root != 0)
    {
      graphAux(indent + 8, root->right);
      cout << setw(indent) << " " << root->key << endl;
      graphAux(indent + 8, root->left);
    }
}

//*** Height of a tree (including max function)
template <class DT>
int Tree<DT>::max(int first, int second)
{
    if(first > second)
	{
		return first;
	}
	else
	{
		return second;
	}       
}

template <class DT>
int Tree<DT>::height(treeNode*root)
{
    if(root == NULL)
	{
		return 0;
	}
	else
	{
		return 1 + max(height(root->left), height(root->right));
	}
}
